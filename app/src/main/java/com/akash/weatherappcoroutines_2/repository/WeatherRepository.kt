package com.akash.weatherappcoroutines_2.repository

import com.akash.weatherappcoroutines_2.api.ApiService
import javax.inject.Inject

class WeatherRepository
@Inject
constructor(private val apiService: ApiService) {
    suspend fun getWeather() = apiService.getWeather()
}