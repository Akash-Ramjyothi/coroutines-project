package com.akash.weatherappcoroutines_2

import android.app.Application
import dagger.hilt.android.HiltAndroidApp
import kotlin.text.Typography.dagger

@HiltAndroidApp
class MyApplication : Application()