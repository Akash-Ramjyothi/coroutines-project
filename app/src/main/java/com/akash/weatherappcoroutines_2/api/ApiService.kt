package com.akash.weatherappcoroutines_2.api

import com.akash.weatherappcoroutines_2.Constants
import com.akash.weatherappcoroutines_2.model.Weather
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("weather/Mumbai")
    suspend fun getWeather(): Response<Weather>

}