package com.akash.weatherappcoroutines_2.model


data class Forecast(
    val day: String,
    val temperature: String,
    val wind: String
)